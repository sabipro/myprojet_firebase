import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class User extends Equatable {
  //determination des attributs du model
  final String? id;
  final String nom;
  final String prenom;
  final int age;
  //constructeur
  const User({
    required this.id,
    this.nom = '',
    this.prenom = '',
    this.age = 0,
  });

// methode pour convertir lun objet en collection c'est a dire en json
  static User fromSnapshot(DocumentSnapshot data) {
    User user = User(
      id: data.id,
      nom: data.data().toString().contains("nom") ? data["nom"] : '',
      prenom: data.data().toString().contains("prenom") ? data["prenom"] : '',
      age: data.data().toString().contains("age") ? data["age"] : '',
    );
    print("mesage usermodel: $user");
    return user;
  }

  // methode qui permet de convertir un object en collection

  @override
  List<Object?> get props => [id, nom, prenom, age];
}
