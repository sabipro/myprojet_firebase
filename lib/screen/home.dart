// import 'package:flutter/material.dart';
// import 'package:flutter_project/models/userModel.dart';
// import 'package:flutter_project/ripository/user_ripository.dart';

// class Home extends StatelessWidget {
//   const Home({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     UserRipository userRipo = UserRipository();
//     final TextEditingController controllerName = TextEditingController();
//     final TextEditingController controllerprenom = TextEditingController();
//     final TextEditingController controllerAge = TextEditingController();
//     return Center(
//         child: Form(
//             child: Column(
//       children: [
//         TextField(
//           controller: controllerName,
//           decoration: const InputDecoration(
//             icon: Icon(Icons.person),
//             hintText: "nom",
//             label: Text("Votre nom"),
//           ),
//         ),
//         const SizedBox(
//           height: 10,
//         ),
//         TextField(
//           controller: controllerprenom,
//           decoration: const InputDecoration(
//             icon: Icon(Icons.person),
//             hintText: "prenom",
//             label: Text("Votre prenom"),
//           ),
//         ),
//         const SizedBox(
//           height: 10,
//         ),
//         TextField(
//           controller: controllerAge,
//           keyboardType: TextInputType.number,
//           decoration: const InputDecoration(
//             icon: Icon(Icons.brightness_high_rounded),
//             hintText: "age",
//             label: Text("votre age"),
//           ),
//         ),
//         const SizedBox(
//           height: 10,
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           children: [
//             ElevatedButton(
//                 onPressed: () {
//                   User user = User(
//                       nom: controllerName.text,
//                       prenom: controllerprenom.text,
//                       age: int.parse(controllerAge.text));

//                   userRipo.addUser(user);
//                   controllerName.text = '';
//                   controllerprenom.text = '';
//                   controllerAge.text = '';
//                 },
//                 child: const Text("Ajouter")),
//             ElevatedButton(
//                 onPressed: () {
//                   Navigator.pop(context);
//                 },
//                 child: const Text("Annuler"))
//           ],
//         )
//       ],
//     )));
//   }
// }
