

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_project/blocs/user/user_bloc.dart';
import 'package:flutter_project/ripository/user_ripository.dart';

class ListUsers extends StatelessWidget {
  const ListUsers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => UserBloc(
              userRipository: UserRipository(),
            ),
        child: BlocBuilder<UserBloc, UserState>(
          builder: ((context, state) {
            if (state.status == UserStateStatus.loading) {
             return Text(state.listUser.length.toString());
              // return const CircularProgressIndicator();
            } else if (state.status == UserStateStatus.success) {
              return Card(
                child: ListView.builder(
                    itemCount: state.listUser.length,
                    itemBuilder: ((context, index) {
                      return ListTile(
                        title: Text(state.listUser[index].nom),
                      );
                    })),
              );
            } else {
              return const Text("error");
            }
          }),
        ));
  }
}
