// import 'package:flutter/material.dart';
// import 'package:flutter_project/screen/list_user.dart';
// import 'package:flutter_project/ripository/user_ripository.dart';
// import '../models/userModel.dart';

// class ListUsers extends StatefulWidget {
//   List<User> users;
//   ListUsers({required this.users});

//   @override
//   State<ListUsers> createState() => _ListUsersState();
// }

// class _ListUsersState extends State<ListUsers> {
//   final controllerNom = TextEditingController();
//   final controllerAge = TextEditingController();
//   final controllerPrenom = TextEditingController();
//   // @override
//   // void initState() {
//   //   super.initState();
//   //   controllerNom.text = widget.users[0].nom;
//   // }

//   @override
//   Widget build(BuildContext context) {
//     final allUsers = widget.users;
//     //creer des controller
//     //instancier
//     UserRipository userRipository = UserRipository();
//     // final controllerNom = TextEditingController();

//     return ListView.builder(
//         itemCount: allUsers.length,
//         itemBuilder: (context, index) {
//           return Card(
//             child: ListTile(
//                 leading: CircleAvatar(
//                   backgroundColor: Colors.green[600],
//                   child: Text(
//                     allUsers[index].nom.substring(0, 2).toUpperCase(),
//                     style: const TextStyle(
//                         fontSize: 25, fontWeight: FontWeight.w800),
//                   ),
//                 ),
//                 title: Text(
//                   allUsers[index].prenom,
//                   style: const TextStyle(fontWeight: FontWeight.bold),
//                 ),
//                 subtitle: Text(allUsers[index].age.toString()),
//                 trailing: Row(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     TextButton(
//                         onPressed: () {
//                           showDialog(
//                               barrierDismissible: false,
//                               context: context,
//                               builder: (BuildContext) {
//                                 return AlertDialog(
//                                   actions: [
//                                     ElevatedButton(
//                                         onPressed: () {
//                                           Navigator.pop(context);
//                                         },
//                                         child: const Text("Annuler"))
//                                   ],
//                                   title: Text('Update: ${allUsers[index].nom}'),
//                                   content: SingleChildScrollView(
//                                     child: Container(
//                                         height: 250,
//                                         child: Column(
//                                           children: [
//                                             TextField(
//                                               controller: controllerNom,
//                                               style: const TextStyle(
//                                                   fontSize: 20,
//                                                   color: Colors.green),
//                                               decoration: const InputDecoration(
//                                                   labelText: "nom",
//                                                   border: OutlineInputBorder()),
//                                             ),
//                                             const SizedBox(
//                                               height: 10,
//                                             ),
//                                             TextField(
//                                               controller: controllerPrenom,
//                                               style: const TextStyle(
//                                                   fontSize: 20,
//                                                   color: Colors.green),
//                                               decoration: const InputDecoration(
//                                                   labelText: "prenom",
//                                                   border: OutlineInputBorder()),
//                                             ),
//                                             const SizedBox(
//                                               height: 10,
//                                             ),
//                                             TextField(
//                                               controller: controllerAge,
//                                               style: const TextStyle(
//                                                   fontSize: 20,
//                                                   color: Colors.green),
//                                               decoration: const InputDecoration(
//                                                   labelText: "age",
//                                                   border: OutlineInputBorder()),
//                                             ),
//                                             const SizedBox(
//                                               height: 10,
//                                             ),
//                                             ElevatedButton(
//                                                 onPressed: () {
//                                                   // on cree d'abord l'objet user avec son id
//                                                   final users = User(
//                                                       id: allUsers[index].id,
//                                                       nom: controllerNom.text,
//                                                       prenom:
//                                                           controllerPrenom.text,
//                                                       age: int.parse(
//                                                           controllerAge.text));
//                                                   userRipository
//                                                       .updateUser(users);
//                                                 },
//                                                 child: const Text(
//                                                   "Update",
//                                                   style: TextStyle(
//                                                       fontSize: 20,
//                                                       color: Colors.white),
//                                                 ))
//                                           ],
//                                         )),
//                                   ),
//                                 );
//                               });
//                         },
//                         child: const Icon(
//                           Icons.edit,
//                           size: 32,
//                           color: Colors.green,
//                         )),
//                     TextButton(
//                         onPressed: () {
//                           // userRipository.deleteUser(allUsers[index].id);
//                           print(' uuesrId :${userRipository.deleteUser(allUsers[index].id)}');
//                         },
//                         child: const Icon(
//                           Icons.delete,
//                           size: 32,
//                           color: Colors.red,
//                         )),
//                   ],
//                 )),
//           );
//         });
//   }
// }
