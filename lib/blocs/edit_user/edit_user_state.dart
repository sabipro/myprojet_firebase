part of 'edit_user_bloc.dart';

abstract class EditUserState extends Equatable {
  const EditUserState();
  
  @override
  List<Object> get props => [];
}

class EditUserInitial extends EditUserState {}
