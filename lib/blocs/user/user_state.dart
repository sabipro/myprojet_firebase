part of 'user_bloc.dart';

enum UserStateStatus {initial, loading, success, faillure }

class UserState extends Equatable {
  
  const  UserState(
      {this.status = UserStateStatus.loading, this.listUser = const <User>[],
      }
      );
  final UserStateStatus status;
  final List<User> listUser;

  UserState copyWith({
    UserStateStatus? status,
    List<User>? listUser,
  }) {
    return UserState(
      status: status ?? this.status,
      listUser: listUser ?? this.listUser,
    );
  }

  @override
  List<Object?> get props => [status, listUser];
}
