import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_project/ripository/user_ripository.dart';

import '../../models/userModel.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRipository _userRipository;
  StreamSubscription? _subscription;
  UserBloc({required UserRipository userRipository})
      : _userRipository = userRipository,
        // ignore: prefer_const_constructors
        super(UserState()) {
    on<GetAllUserChanged>(_getAllUsers);
    on<DeleteUserChanged>(_deleteUser);
    on<LoadUser>(_loadUser);
  }

  FutureOr<void> _loadUser(LoadUser event, Emitter<UserState> emit) {
    _subscription?.cancel();
    _subscription = _userRipository
        .getAllUser()
        .listen((event) => add(GetAllUserChanged(event)));
  }

  FutureOr<void> _getAllUsers(
    GetAllUserChanged event,
    Emitter<UserState> emit,
  ) {
    emit(state.copyWith(listUser: event.allUser));
    emit(state.copyWith(status: UserStateStatus.success));
  }

  FutureOr<void> _deleteUser(
    DeleteUserChanged event,
    Emitter<UserState> emit,
  ) {
    _userRipository.deleteUser(event.id);
    emit(state.copyWith(status: UserStateStatus.success));
  }
}
