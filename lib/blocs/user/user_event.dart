part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();

  @override
  List<Object> get props => [];
}

class GetAllUserChanged extends UserEvent {
  final List<User> allUser;
  const GetAllUserChanged(this.allUser);
  
  @override
  List<Object> get props => [allUser];
}

class DeleteUserChanged extends UserEvent {
  final String id;
  const DeleteUserChanged(this.id);

  @override
  List<Object> get props => [id];
}
class LoadUser extends UserEvent{}
