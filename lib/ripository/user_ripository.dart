//ici ou je cree mes requette

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_project/models/userModel.dart';
import 'package:flutter_project/ripository/base_user_ripository.dart';

class UserRipository extends BaseUserRipository {
  final FirebaseFirestore _firebaseFirestore;
  UserRipository({FirebaseFirestore? firebaseFirestore})
      : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  @override
  Future<void> deleteUser(String id) {
    return _firebaseFirestore.collection("user").doc(id).delete();
  }

  @override
  Stream<List<User>> getAllUser() {
    return _firebaseFirestore.collection("user").snapshots().map((snapshot) {
      return snapshot.docs.map((e) => User.fromSnapshot(e)).toList();
    });
  }

  @override
  Future<void> saveUser(User user, String id) {
    if (user.id == '') {
      return _firebaseFirestore.collection("user").add({
        'nom': user.nom,
        'prenom': user.prenom,
        'age': user.age,
      });
    } else {
      return _firebaseFirestore
          .collection("user")
          .doc(id)
          .update({'nom': user.nom, 'prenom': user.prenom, 'age': user.age});
    }
  }
}
