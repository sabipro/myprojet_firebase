import 'package:flutter_project/models/userModel.dart';

 abstract class BaseUserRipository {
  Future<void> saveUser(User user,String id);

  Stream<List<User>> getAllUser();

  Future<void> deleteUser(String id);

}
//j'ai definis une methode pour recupere les data dans firebase
